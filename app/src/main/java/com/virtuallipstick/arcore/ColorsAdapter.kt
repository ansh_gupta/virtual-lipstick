package com.virtuallipstick.arcore

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView

class ColorsAdapter(private val colors: MutableList<String>, private val context: Context?, private val listener: (Int) -> Unit) :
    RecyclerView.Adapter<ColorsAdapter.ColorPill>() {

    private var selectedColorIndex: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorPill {

        val view = LayoutInflater.from(context).inflate(R.layout.color_pill, parent, false)
        return ColorPill(view)
    }

    override fun onBindViewHolder(colorPill: ColorPill, @SuppressLint("RecyclerView") position: Int) {
        colorPill.color.setColorFilter(Color.parseColor(colors[position]))

        if(position == selectedColorIndex){
            colorPill.colorContainer.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#FF3F6C"))
        }
        else
            colorPill.colorContainer.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#FFFFFF"))

        colorPill.itemView.setOnClickListener {
            selectedColorIndex = position
            listener(position)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        var arr = 0
        try {
            arr = if (colors.isEmpty()) {
                0
            } else {
                colors.size
            }
        }
        catch (ignored: Exception) { }
        return arr
    }

    inner class ColorPill(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var color: ImageView = itemView.findViewById(R.id.color)
        var colorContainer: ConstraintLayout = itemView.findViewById(R.id.color_container)
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }
}