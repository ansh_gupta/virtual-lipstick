package com.virtuallipstick.arcore

import android.app.ActivityManager
import android.content.Context
import android.graphics.*
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.ar.core.ArCoreApk
import com.google.ar.core.AugmentedFace
import com.google.ar.core.TrackingState
import com.google.ar.sceneform.rendering.Renderable
import com.google.ar.sceneform.rendering.Texture
import com.google.ar.sceneform.ux.AugmentedFaceNode
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.collections.set


class MainActivity : AppCompatActivity() {
    companion object {
        const val MIN_OPENGL_VERSION = 3.0
    }

    private lateinit var arFragment: FaceArFragment
    private lateinit var colorSelector: RecyclerView
    private var faceNodeMap = HashMap<AugmentedFace, AugmentedFaceNode>()
    private var lipstick: Texture ?= null
    private var selectedColorInd: Int = 0
    private lateinit var colorsAdapter: ColorsAdapter
    private var colors: MutableList<String> = arrayListOf()
    private var changeTexture: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (!checkIsSupportedDeviceOrFinish()) {
            return
        }

        setContentView(R.layout.activity_main)
        arFragment = face_fragment as FaceArFragment
        colorSelector = colour_selector as RecyclerView

        colors.add("#5B3128")
        colors.add("#00574B")
        colors.add("#e11b4a")
        colors.add("#353E63")
        colors.add("#382E38")
        colors.add("#8143dc")
        colors.add("#ffffff")
        colors.add("#000000")

        setAdapter()

        getTexture()

        val sceneView = arFragment.arSceneView
        sceneView.cameraStreamRenderPriority = Renderable.RENDER_PRIORITY_FIRST
        val scene = sceneView.scene

        scene.addOnUpdateListener {
            sceneView.session
                ?.getAllTrackables(AugmentedFace::class.java)?.let {
                    for (f in it) {
                        if (!faceNodeMap.containsKey(f)) {
                            val faceNode = AugmentedFaceNode(f)
                            faceNode.setParent(scene)
                            faceNode.faceMeshTexture = lipstick
                            faceNodeMap[f] = faceNode
                        }
                        else if (changeTexture)
                            faceNodeMap.getValue(f).faceMeshTexture = lipstick
                    }
                    changeTexture = false

                    val iter = faceNodeMap.entries.iterator()
                    while (iter.hasNext()) {
                        val entry = iter.next()
                        val face = entry.key
                        if (face.trackingState == TrackingState.STOPPED) {
                            val faceNode = entry.value
                            faceNode.setParent(null)
                            iter.remove()
                        }
                    }
                }
        }
    }

    private fun getTexture() {
        Texture.builder()
            .setUsage(Texture.Usage.COLOR)
            .setSource(getBitmap(colors[selectedColorInd]))
            .build()
            .thenAccept { texture ->
                lipstick = texture
                changeTexture = !changeTexture
            }
    }

    private fun changeBitmapColor(bitmap: Bitmap, color: String): Bitmap {
        val paint = Paint()
        val canvas = Canvas(bitmap)
        val filter = LightingColorFilter(1, Color.parseColor(color))
        paint.colorFilter = filter
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC)
        canvas.drawBitmap(bitmap, 0f, 0f, paint)

        return bitmap
    }

    private fun getBitmap(color: String): Bitmap {
        val options: BitmapFactory.Options = BitmapFactory.Options()
        options.inMutable = true
        val bp: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.lips3, options)

        return changeBitmapColor(bp, color)
    }


    private fun updateColor(selectedColorIndex: Int) {
        selectedColorInd = selectedColorIndex
        getTexture()
    }

    private fun setAdapter(){

        colorsAdapter = ColorsAdapter(colors, applicationContext) { position ->
            updateColor(position)
        }
        val layout = LinearLayoutManager(applicationContext, LinearLayoutManager.HORIZONTAL, false)
        colorSelector.layoutManager = layout
        colorSelector.itemAnimator = DefaultItemAnimator()
        colorSelector.adapter = colorsAdapter

    }

    private fun checkIsSupportedDeviceOrFinish() : Boolean {
        if (ArCoreApk.getInstance().checkAvailability(this) == ArCoreApk.Availability.UNSUPPORTED_DEVICE_NOT_CAPABLE) {
            Toast.makeText(this, "Augmented Faces requires ARCore", Toast.LENGTH_LONG).show()
            finish()
            return false
        }
        val openGlVersionString =  (getSystemService(Context.ACTIVITY_SERVICE) as? ActivityManager)
            ?.deviceConfigurationInfo
            ?.glEsVersion

        openGlVersionString?.let {
            if (java.lang.Double.parseDouble(openGlVersionString) < MIN_OPENGL_VERSION) {
                Toast.makeText(this, "Sceneform requires OpenGL ES 3.0 or later", Toast.LENGTH_LONG)
                    .show()
                finish()
                return false
            }
        }
        return true
    }

}